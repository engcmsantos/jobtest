#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

xQueueHandle commQueue;
static void producer( void *pvParameters );
static void Consumer( void *pvParameters );

#define QUEUE_ITEM_SIZE 1
#define QUEUE_LENGTH    5

class classQueue{
	unsigned char elementQueue;
	int count_elements = 0;
	int max_size = QUEUE_LENGTH
	myQueue = xQueueCreate(max_size,QUEUE_ITEM_SIZE);
	
	void Producer(void *pvParameters){//Push
		while(1)
		{
			if(this.count_elements<=max_size){
				i++;
				xQueueSend(myQueue,&i,20);
			}
			vTaskDelay(20);
		}
	}
	
	static void Consumer( void *pvParameters ){//Pop

		while(1)
		{
			if(this.count_elements>=0){
				xQueueReceive(myQueue,&c,20);
				i--;
			}
		}
	}
}

void main(void)
{
  newQueue = new classQueue;
  
  xTaskCreate( classQueue.Producer,         
              (const char*) "Producer", 
              QUEUE_LENGTH,                
              NULL,                    
              tskIDLE_PRIORITY + 1,     
              NULL );                   
  
  xTaskCreate(classQueue.Consumer,               
			  (const char*)"Consumer",
              QUEUE_LENGTH,            
              NULL,                    
              tskIDLE_PRIORITY + 1,    
              NULL );                  
  
  /* Inicialização do escalonador */
  vTaskStartScheduler();
  return ;
}