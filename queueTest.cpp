#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define QUEUE_ITEM_SIZE 1
#define QUEUE_LENGTH    5

class classQueue{
	int count_elements = 0;
	int max_size = QUEUE_LENGTH
	
	xQueueHandle myQueue = xQueueCreate(max_size,QUEUE_ITEM_SIZE);
	
	void Producer(int *elementQueue){//Push
		while(1)
		{
			if(this.count_elements<=max_size){
				count_elements++;
				xQueueSend(myQueue,&elementQueue,20);
			}
			vTaskDelay(20);
		}
	}
	
	static void Consumer(int *elementQueue){//Pop

		while(1)
		{
			if(this.count_elements>=0){
				xQueueReceive(myQueue,&elementQueue,20);
				count_elements--;
			}
		}
	}
}

TEST_GROUP(Queue_test)
{
	void setup(){
		xTaskCreate( classQueue.Producer,         
              (const char*) "Producer", 
              QUEUE_LENGTH,                
              NULL,                    
              tskIDLE_PRIORITY + 1,     
              NULL );                   
  
		xTaskCreate(classQueue.Consumer,               
			  (const char*)"Consumer",
              QUEUE_LENGTH,            
              NULL,                    
              tskIDLE_PRIORITY + 1,    
              NULL );                  
  
		vTaskStartScheduler();
	}
	
	void teardown()
    {
        mock().clear();
    }
};

TEST(Queue_test, newQueue_ok)
{
	newQueue = new classQueue;
	int a = 1;
	int b = 2;
	int *buffer,
	newQueue.Producer(a);
	newQueue.Producer(b);
	CHECK_EQUAL(2,count_elements);
	newQueue.Consumer(&buffer);
	CHECK_EQUAL(a,count_elements);
	CHECK_EQUAL(1,count_elements);
	newQueue.Consumer(&buffer);
	CHECK_EQUAL(false, newQueue.Consumer(&buffer);
}